//
// Created by pbialas on 25.09.2020.
//

#include "app.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


#include "Application/utils.h"

void SimpleShapeApplication::init() {


    auto program = xe::create_program(std::string(PROJECT_DIR) + "/shaders/base_vs.glsl",
                                      std::string(PROJECT_DIR) + "/shaders/base_fs.glsl");


    if (!program) {
        std::cerr << "Cannot create program from " << std::string(PROJECT_DIR) + "/shaders/base_vs.glsl" << " and ";
        std::cerr << std::string(PROJECT_DIR) + "/shaders/base_fs.glsl" << " shader files" << std::endl;
    }

    std::vector<GLfloat> vertices = {
            -0.5f, -0.5f, 0.5f, 0.8f, 0.1f, 0.8f,
            0.5f, -0.5f, 0.5f, 0.8f, 0.1f, 0.8f,
            0.0f, 0.5f, 0.0f, 0.8f, 0.1f, 0.8f, // here ends first triangle

            0.5f, -0.5f, 0.5f, 0.1f, 0.8f, 0.8f,
            0.5f, -0.5f, -0.5f, 0.1f, 0.8f, 0.8f,
            0.0f, 0.5f, 0.0f, 0.1f, 0.8f, 0.8f, // here ends second triangle

            0.5f, -0.5f, -0.5f, 0.2f, 0.2f, 0.8f,
            -0.5f, -0.5f, -0.5f, 0.2f, 0.2f, 0.8f,
            0.0f, 0.5f, 0.0f, 0.2f, 0.2f, 0.8f, // here ends third triangle

            -0.5f, -0.5f, -0.5f, 0.8f, 0.8f, 0.3f,
            -0.5f, -0.5f, 0.5f, 0.8f, 0.8f, 0.3f,
            0.0f, 0.5f, 0.0f, 0.8f, 0.8f, 0.3f, // here ends fourth triangle

            -0.5f, -0.5f, 0.5f, 0.3f, 0.1f, 0.3f,
            0.5f, -0.5f, 0.5f, 0.3f, 0.1f, 0.3f,
            -0.5f, -0.5f, -0.5f, 0.3f, 0.1f, 0.3f, // here ends base 1 - ABD

//            -0.5f, -0.5f, -0.5f, 0.3f, 0.1f, 0.3f,
//            0.5f, -0.5f, 0.5f, 0.3f, 0.1f, 0.3f,
            0.5f, -0.5f, -0.5f, 0.3f, 0.1f, 0.3f, // here ends base 2 - DBC
    };

    std::vector<GLushort> indices = {
            0, 1, 2,
            3, 4, 5,
            6, 7, 8,
            9, 10, 11,
            12, 13, 14,
            14, 13, 15
    };

    GLuint idx_buffer_handle;
    glGenBuffers(1, &idx_buffer_handle);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idx_buffer_handle);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLushort), indices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    GLuint v_buffer_handle;
    glGenBuffers(1, &v_buffer_handle);
    glBindBuffer(GL_ARRAY_BUFFER, v_buffer_handle);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    glGenVertexArrays(1, &vao_);
    glBindVertexArray(vao_);
    glBindBuffer(GL_ARRAY_BUFFER, v_buffer_handle);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idx_buffer_handle);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), reinterpret_cast<GLvoid *>(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
                          reinterpret_cast<GLvoid *>(3 * sizeof(GLfloat)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);


    auto u_transformations_index = glGetUniformBlockIndex(program, "Transformations");
    auto transformations_binding_point = 0;
    if (u_transformations_index == GL_INVALID_INDEX) {
        std::cout << "Cannot find Transformations uniform block in program" << std::endl;
    } else {
        glUniformBlockBinding(program, u_transformations_index, transformations_binding_point);
    }

    int w, h;
    std::tie(w, h) = frame_buffer_size();
    aspect_ = (float)w/(float)h;
    fov_ = glm::radians(45.0f);
    near_ = 0.1f;
    far_ = 100.f;

    const glm::mat4 I(1.0f);
    M_ = glm::translate(I, glm::vec3(0.0f, 0.0f, 2.0f));

    P_ = glm::perspective(fov_, aspect_, near_, far_);
    // eye = eye position = camera position
    // center = DIRECTION OF CAMERA = TARGET = WHERE CAM IS LOOKING
    V_ = glm::lookAt(
            glm::vec3(10.0f, 4.0f, 1.0f),
            glm::vec3(0.0f, 0.0f, 1.0f),
            glm::vec3(0.0f, 1.0f, 0.0f)
    );


    glGenBuffers(1, &u_pvm_buffer);
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, u_pvm_buffer);

    auto modifiers_binding_point = 1;
    auto u_modifiers_index = glGetUniformBlockIndex(program, "Modifiers");
    if (u_modifiers_index == GL_INVALID_INDEX) {
        std::cout << "Cannot find Modifiers uniform block in program" << std::endl;
    } else {
        glUniformBlockBinding(program, u_modifiers_index, modifiers_binding_point);
    }

    float light_intensity = 0.92f;
    float light_color[3] = {0.9f, 0.9f, 0.9f};

    GLuint ubo_handle(0u);
    glGenBuffers(1, &ubo_handle);
    glBindBuffer(GL_UNIFORM_BUFFER, ubo_handle);
    glBufferData(GL_UNIFORM_BUFFER, 8 * sizeof(float), nullptr, GL_STATIC_DRAW);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(float), &light_intensity);
    glBufferSubData(GL_UNIFORM_BUFFER, 4 * sizeof(float), 3 * sizeof(float), light_color);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, modifiers_binding_point, ubo_handle);

    glBindBuffer(GL_UNIFORM_BUFFER, u_pvm_buffer);
    glBufferData(GL_UNIFORM_BUFFER, 4 * 4 * sizeof(float), nullptr, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    glClearColor(0.81f, 0.81f, 0.8f, 1.0f);
    glViewport(0, 0, w, h);


    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glUseProgram(program);
}

void SimpleShapeApplication::frame() {
    glBindVertexArray(vao_);

    const glm::mat4 PVM = P_ * V_ * M_;
    glBindBuffer(GL_UNIFORM_BUFFER, u_pvm_buffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, 4 * 4 * sizeof(float), &PVM[0]);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    int number_of_elements = 6 * 3;
    glDrawElements(GL_TRIANGLES, number_of_elements, GL_UNSIGNED_SHORT, nullptr);
    glBindVertexArray(0);
}

void SimpleShapeApplication::framebuffer_resize_callback(int w, int h) {
    Application::framebuffer_resize_callback(w, h);
    glViewport(0,0,w,h);
    aspect_ = (float) w / (float)h;
    P_ = glm::perspective(fov_, aspect_, near_, far_);
}

