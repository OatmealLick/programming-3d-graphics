//
// Created by lick on 18.11.2020.
//

#pragma once

#include "glad/glad.h"
#include "phong_material.h"
#include <vector>


class Quad  {
public:
    Quad();
    ~Quad() = default;
    void draw();
    void set_material(PhongMaterial *material) { material_ = material; }
    PhongMaterial* get_material() {
        return material_;
    }
private:
    GLuint diffuse_texture_;
    GLuint vao_;
    GLuint buffer_[2];
    PhongMaterial *material_;
    std::vector<GLfloat> vertices = {
            -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
            1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
            1.0f, 0.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
            -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f
    };

    std::vector<GLushort> indices = {
            0, 1, 2,
            2, 3, 0
    };
};