//
// Created by pbialas on 25.09.2020.
//

#include "app.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <3rdParty/src/stb/stb_image.h>

#include "quad.h"

void SimpleShapeApplication::init() {


    auto program = xe::create_program(std::string(PROJECT_DIR) + "/shaders/base_vs.glsl", std::string(PROJECT_DIR) + "/shaders/base_fs.glsl");

    if (!program) {
        std::cerr << "Cannot create program from " << std::string(PROJECT_DIR) + "/shaders/base_vs.glsl" << " and ";
        std::cerr << std::string(PROJECT_DIR) + "/shaders/base_fs.glsl" << " shader files" << "\n";
    }

    xe::utils::set_uniform_block_binding(program, "Transformations", 0);
    xe::utils::set_uniform_block_binding(program, "Light", 1);
    xe::utils::set_uniform_block_binding(program, "Material", 2);

    glGenBuffers(1, &u_pvm_buffer);
    glBindBuffer(GL_UNIFORM_BUFFER, u_pvm_buffer);
    glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4) + 3 * sizeof(glm::vec4), nullptr, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, u_pvm_buffer);

    glGenBuffers(1, &u_light_buffer);
    glBindBuffer(GL_UNIFORM_BUFFER, u_light_buffer);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(Light), nullptr, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, u_light_buffer);

    glGenBuffers(1, &u_material_buffer);
    glBindBuffer(GL_UNIFORM_BUFFER, u_material_buffer);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(PhongMaterial), nullptr, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, 2, u_material_buffer);

    light_.position = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
    light_.color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    light_.a = glm::vec4(1.0f, 0.0f, 1.0f, 0.0f);
    light_.ambient = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

    glClearColor(0.1f, 0.4f, 0.1f, 1.0f);
    int w, h;
    std::tie(w, h) = frame_buffer_size();
    camera_ = new Camera();
    controller_ = new CameraController(camera_);
    camera_->perspective(glm::radians(40.0f), (float)w / (float)h, 0.1f, 100.0f);
    camera_->look_at(
            (glm::vec3) {0.0f, 4.0f, 0.0f},
            (glm::vec3) {0.0f, 0.0f, 0.0f},
            (glm::vec3) {0.1f, 1.0f, 0.0f});

    GLuint textures_[2]{ 0, 0};
    glGenTextures(2, textures_);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures_[0]);
    xe::utils::load_texture(std::string(PROJECT_DIR) + "/Textures/plastic.png");
    glBindTexture(GL_TEXTURE_2D, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textures_[1]);
    xe::utils::load_texture(std::string(PROJECT_DIR) + "/Textures/shininess.png");
    glBindTexture(GL_TEXTURE_2D, 0);

    quad = new Quad();
    auto mat_ = new PhongMaterial();
    mat_->Kd = glm::vec3(1.0, 1.0, 1.0);
    mat_->Kd_map = textures_[0];
    mat_->Ks = glm::vec3(0.05, 0.85, 0.85);
    mat_->Ks_map = 0;
    mat_->Ns = 1000.0f;
    mat_->Ns_map = textures_[1];
    quad->set_material(mat_);

    glViewport(0, 0, w, h);

    glEnable(GL_DEPTH_TEST);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glUseProgram(program);

    // Textures
    xe::utils::set_uniform1i(program, "diffuse_map", 0);
    xe::utils::set_uniform1i(program,"specular_map",1);
    xe::utils::set_uniform1i(program,"shininess_map",2);
}

void SimpleShapeApplication::frame() {
    glm::mat4 P = camera_->projection();
    auto M_ = glm::mat4(1.0f);
    glm::mat4 VM = M_ * camera_->view();
    glm::mat4 N = glm::transpose(glm::inverse(glm::mat3(VM)));
    light_.position_in_vs = VM * light_.position;

    glBindBuffer(GL_UNIFORM_BUFFER, u_pvm_buffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, 4 * 4 * sizeof(float), &P[0]);
    glBufferSubData(GL_UNIFORM_BUFFER, 4 * 4 * sizeof(float), 4 * 4 * sizeof(float), &VM[0]);
    glBufferSubData(GL_UNIFORM_BUFFER, 8 * 4 * sizeof(float), 3 * sizeof(float), &N[0]);
    glBufferSubData(GL_UNIFORM_BUFFER, (8 + 1) * 4 * sizeof(float), 3 * sizeof(float), &N[1]);
    glBufferSubData(GL_UNIFORM_BUFFER, (8 + 2) * 4 * sizeof(float), 3 * sizeof(float), &N[2]);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    glBindBuffer(GL_UNIFORM_BUFFER, u_light_buffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, 4 * sizeof(glm::vec4), &light_.position_in_vs[0]);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    glBindBuffer(GL_UNIFORM_BUFFER, u_material_buffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(PhongMaterial), quad->get_material());
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    auto material = quad->get_material();
    if(material->Kd_map>0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, material->Kd_map);
    }
    if(material->Ks_map>0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, material->Ks_map);
    }
    if(material->Ns_map>0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, material->Ns_map);
    }
    quad->draw();
    glBindTexture(GL_TEXTURE_2D, 0);

}

void SimpleShapeApplication::framebuffer_resize_callback(int w, int h) {
    Application::framebuffer_resize_callback(w, h);
    glViewport(0, 0, w, h);
    const float aspect_ = (float) w / (float) h;
    camera_->set_aspect(aspect_);
}

void SimpleShapeApplication::scroll_callback(double xoffset, double yoffset) {
    Application::scroll_callback(xoffset, yoffset);
    camera_->zoom(-(float) yoffset / 30.0f);
}

void SimpleShapeApplication::mouse_button_callback(int button, int action, int mods) {
    Application::mouse_button_callback(button, action, mods);

    if (controller_) {
        double x, y;
        glfwGetCursorPos(window_, &x, &y);

        if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
            controller_->LMB_pressed((float) x, (float) y);

        if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
            controller_->LMB_released((float) x, (float) y);
    }

}

void SimpleShapeApplication::cursor_position_callback(double x, double y) {
    Application::cursor_position_callback(x, y);

    if (controller_) {
        controller_->mouse_moved((float) x, (float) y);
    }
}

void SimpleShapeApplication::cleanup() {
    delete quad;
}

SimpleShapeApplication::~SimpleShapeApplication() {
    if (camera_) {
        delete camera_;
    }
}