//
// Created by lick on 12.11.2020.
//

#pragma once

#include "camera.h"

class CameraController {
public:
    CameraController(Camera *camera) : camera_(camera),
                                       scale_(0.015f),
                                       LMB_pressed_(false) {}

    void rotate_camera(float dx, float dy);

    void mouse_moved(float x, float y);

    void LMB_pressed(float x, float y);

    void LMB_released(float x, float y);

private:
    Camera *camera_;
    float scale_;
    bool LMB_pressed_;
    float x_;
    float y_;
};