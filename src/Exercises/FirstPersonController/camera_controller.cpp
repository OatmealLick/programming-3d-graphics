//
// Created by lick on 14.11.2020.
//

#include "camera_controller.h"

void CameraController::rotate_camera(float dx, float dy) {
    camera_->rotate_around_center(-scale_ * dy, camera_->x());
//    camera_->rotate_around_center(-scale_ * dx, glm::vec3 {0.0f, 1.0f, 0.0f});
    camera_->rotate_around_center(-scale_ * dx, camera_->y());
}

void CameraController::mouse_moved(float x, float y) {
    auto dx = x - x_;
    auto dy = y - y_;
    x_ = x;
    y_ = y;

    rotate_camera(dx, dy);
}

void CameraController::LMB_pressed(float x, float y) {
    LMB_pressed_ = true;
    x_ = x;
    y_ = y;
}

void CameraController::LMB_released(float x, float y) {
    LMB_pressed_ = false;
    auto dx = x - x_;
    auto dy = y - y_;

    rotate_camera(dx, dy);
}
