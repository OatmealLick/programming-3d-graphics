//
// Created by pbialas on 25.09.2020.
//

#include "app.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Application/utils.h"
#include "quad.h"

void SimpleShapeApplication::init() {


    auto program = xe::create_program(std::string(PROJECT_DIR) + "/shaders/base_vs.glsl",
                                      std::string(PROJECT_DIR) + "/shaders/base_fs.glsl");


    if (!program) {
        std::cerr << "Cannot create program from " << std::string(PROJECT_DIR) + "/shaders/base_vs.glsl" << " and ";
        std::cerr << std::string(PROJECT_DIR) + "/shaders/base_fs.glsl" << " shader files" << std::endl;
    }

    auto *c = new Camera();
    set_camera(c);
    set_controller(new CameraController(c));
    quad = new Quad();

    auto u_transformations_index = glGetUniformBlockIndex(program, "Transformations");
    auto transformations_binding_point = 0;
    if (u_transformations_index == GL_INVALID_INDEX) {
        std::cout << "Cannot find Transformations uniform block in program" << std::endl;
    } else {
        glUniformBlockBinding(program, u_transformations_index, transformations_binding_point);
    }

    auto u_light_index = glGetUniformBlockIndex(program, "Light");
    auto light_binding_point = 1;
    if (u_light_index == GL_INVALID_INDEX) {
        std::cout << "Cannot find Light uniform block in program" << std::endl;
    } else {
        glUniformBlockBinding(program, u_light_index, light_binding_point);
    }

    light_.color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    light_.a = glm::vec4(1.0f, 0.0f, 1.0f, 0.0f);
    light_.ambient = glm::vec3(0.2f, 0.2f, 0.2f);

    int w, h;
    std::tie(w, h) = frame_buffer_size();
    camera_->perspective(glm::radians(45.0f), (float) w / (float) h, 0.1f, 100.0f);
    camera_->look_at(glm::vec3(0.0f, 0.0f, -2.0f),
                     glm::vec3(0.0f, 0.0f, 0.0f),
                     glm::vec3(0.0f, 1.0f, 0.0f));

    glGenBuffers(1, &u_light_buffer);
    glBindBuffer(GL_UNIFORM_BUFFER, u_light_buffer);
    glBufferData(GL_UNIFORM_BUFFER, 3 * 4 * sizeof(float) + 3 * sizeof(float), nullptr, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, u_light_buffer);

    glGenBuffers(1, &u_pvm_buffer);
    glBindBuffer(GL_UNIFORM_BUFFER, u_pvm_buffer);
    glBufferData(GL_UNIFORM_BUFFER, (4 + 4 + 3) * 4 * sizeof(float), nullptr, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, u_pvm_buffer);

    glClearColor(0.1f, 0.2f, 0.1f, 1.0f);
    glViewport(0, 0, w, h);

    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glUseProgram(program);

    auto u_diffuse_map_location = glGetUniformLocation(program, "diffuse_map");
    if (u_diffuse_map_location == -1) {
        std::cerr << "Cannot find uniform diffuse_map\n";
    } else {
        glUniform1ui(u_diffuse_map_location, 0);
    }
}

void SimpleShapeApplication::frame() {
    glm::mat4 P = camera_->projection();
    glm::mat4 VM = camera_->view();
    glm::mat4 N = glm::transpose(glm::inverse(glm::mat3(VM)));
    light_.position = VM * glm::vec4 (0.0f, 0.0f, 1.0f, 1.0f);

    glBindBuffer(GL_UNIFORM_BUFFER, u_pvm_buffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, 4 * 4 * sizeof(float), &P[0]);
    glBufferSubData(GL_UNIFORM_BUFFER, 4 * 4 * sizeof(float), 4 * 4 * sizeof(float), &VM[0]);
    glBufferSubData(GL_UNIFORM_BUFFER, 8 * 4 * sizeof(float), 3 * sizeof(float), &N[0]);
    glBufferSubData(GL_UNIFORM_BUFFER, (8 + 1) * 4 * sizeof(float), 3 * sizeof(float), &N[1]);
    glBufferSubData(GL_UNIFORM_BUFFER, (8 + 2) * 4 * sizeof(float), 3 * sizeof(float), &N[2]);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    quad->draw();

    glBindBuffer(GL_UNIFORM_BUFFER, u_light_buffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, 4 * sizeof(float), &light_.position[0]);
    glBufferSubData(GL_UNIFORM_BUFFER, 4 * sizeof(float), 4 * sizeof(float), &light_.color[0]);
    glBufferSubData(GL_UNIFORM_BUFFER, 2 * 4 * sizeof(float), 4 * sizeof(float), &light_.a[0]);
    glBufferSubData(GL_UNIFORM_BUFFER, 3 * 4 * sizeof(float), 3 * sizeof(float), &light_.ambient[0]);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void SimpleShapeApplication::framebuffer_resize_callback(int w, int h) {
    Application::framebuffer_resize_callback(w, h);
    glViewport(0, 0, w, h);
    const float aspect_ = (float) w / (float) h;
    camera_->set_aspect(aspect_);
}

void SimpleShapeApplication::scroll_callback(double xoffset, double yoffset) {
    Application::scroll_callback(xoffset, yoffset);
    camera_->zoom(-(float) yoffset / 30.0f);
}

void SimpleShapeApplication::mouse_button_callback(int button, int action, int mods) {
    Application::mouse_button_callback(button, action, mods);

    if (controller_) {
        double x, y;
        glfwGetCursorPos(window_, &x, &y);

        if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
            controller_->LMB_pressed((float) x, (float) y);

        if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
            controller_->LMB_released((float) x, (float) y);
    }

}

void SimpleShapeApplication::cursor_position_callback(double x, double y) {
    Application::cursor_position_callback(x, y);

    if (controller_) {
        controller_->mouse_moved((float) x, (float) y);
    }
}

void SimpleShapeApplication::cleanup() {
    delete quad;
}

SimpleShapeApplication::~SimpleShapeApplication() {
    if (camera_) {
        delete camera_;
    }
}