//
// Created by lick on 18.11.2020.
//

#pragma once

#include "glad/glad.h"
#include <vector>


class Pyramid  {
public:
    Pyramid();
    ~Pyramid() = default;
    void draw(glm::mat4 &PVM, GLuint u_pvm_buffer_) const;
private:
    GLuint diffuse_texture_;
    GLuint vao_;
    GLuint buffer_[2]; //todo for now there is no need for it to be field
    std::vector<GLfloat> vertices = {
            -0.5f, -0.5f, 0.5f, 0.5f, 0.191f, // A
            0.5f, -0.5f, 0.5f, 0.809f, 0.5f,  // B
            0.0f, 0.5f, 0.0f, 1.0f, 0.0f,
            0.5f, -0.5f, -0.5f, 0.5f, 0.809f, // C
            0.0f, 0.5f, 0.0f, 1.0f, 1.0f,
            -0.5f, -0.5f, -0.5f, 0.191f, 0.5f,  // D
            0.0f, 0.5f, 0.0f, 0.0f, 1.0f,
            0.0f, 0.5f, 0.0f, 0.0f, 0.0f,
    };

    std::vector<GLushort> indices = {
            0, 1, 2,
            1, 3, 4,
            3, 5, 6,
            5, 0, 7,
            0, 3, 1,
            0, 5, 3
    };
};