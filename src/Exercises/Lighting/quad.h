//
// Created by lick on 18.11.2020.
//

#pragma once

#include "glad/glad.h"
#include <vector>


class Quad  {
public:
    Quad();
    ~Quad() = default;
    void draw();
private:
    GLuint diffuse_texture_;
    GLuint vao_;
    GLuint buffer_[2];
    std::vector<GLfloat> vertices = {
            -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
            1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
            1.0f, 0.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
            -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f
    };

    std::vector<GLushort> indices = {
            0, 1, 2,
            2, 3, 0
    };
};